package zadatak03.Parking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import zadatak03.Parking.model.Place;
import zadatak03.Parking.repository.PlaceRepository;

@Component
public class PlaceService {

	@Autowired
	PlaceRepository placeRepository;
	
	public Page<Place> findAll(Pageable page){
		return placeRepository.findAll(page);
	}
	
	
	public Place freePlace (Place place) {
		List<Place> places = placeRepository.nadjiSlobodnoMesto();
		if(places.size() != 0) {
			Random randomNumber = new Random();
			int number = randomNumber.nextInt(places.size());
		//	Place place1 = places.get(number);
			return places.get(number);
		}
		return null;
	}
	
	public List <Place> foreignCars (){
		List<Place> occupiedPlaces = placeRepository.findPlaceIsFreeFalse();
		for (Place place : occupiedPlaces) {
			System.out.println(place.getRegistration());
		}
    	if(occupiedPlaces.size() == 0) {
    		return null;
    	}
		List<Place> foreigners = new ArrayList<>();
    	for (Place place : occupiedPlaces) {
			
    		if(place.getRegistration() != null && !place.getRegistration().matches("[A-Z]{2}\\d{3}[A-Z]{2}")){
				foreigners.add(place);
			}
		}
    	System.out.println(foreigners.size()); 
    	return foreigners;
    }
	
	public Place save(Place place) {
		return placeRepository.save(place);
	}
	
}
