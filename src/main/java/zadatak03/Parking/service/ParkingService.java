package zadatak03.Parking.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import zadatak03.Parking.model.Parking;
import zadatak03.Parking.repository.ParkingRepository;

@Component
public class ParkingService {

	@Autowired
	ParkingRepository parkingRepository;
	
	public Page<Parking> findAll(Pageable page){
		return parkingRepository.findAll(page);
	}
	public List<Parking> findAll(){
		return parkingRepository.pronadjiSveParkinge();
	}
	public List<Parking> pronadjiPoIdu(Long id){
		return parkingRepository.nadjiParkingPoId(id);
	}
	
	public Parking findById(Long id) {
		return parkingRepository.findOne(id);
	}
	
	public Parking save(Parking parking) {
		return parkingRepository.save(parking);
	}
	public List<Parking> pronadjiSlobodanParking() {
		System.out.println(parkingRepository.pronadjiSlobodanParking().get(0).getName()); 
		return parkingRepository.pronadjiSlobodanParking();
	}
}
