package zadatak03.Parking.web.dto;

import zadatak03.Parking.model.Place;


public class PlaceDTO {
	
	private Long id;
	private int number;
	private boolean isFree;
	private String registration;
	private ParkingDTO parkingDTO;
	
	public PlaceDTO() {
		super();
	}
	
	public PlaceDTO(Place place) {
		this.id = place.getId();
		this.number = place.getNumber();
		this.registration = place.getRegistration();
		this.isFree = place.isFree();
	}

	public PlaceDTO(Long id, int number, boolean isFree, String registration) {
		super();
		this.id = id;
		this.number = number;
		this.isFree = isFree;
		this.registration = registration;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public boolean isFree() {
		return isFree;
	}

	public void setFree(boolean isFree) {
		this.isFree = isFree;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public ParkingDTO getParkingDTO() {
		return parkingDTO;
	}

	public void setParkingDTO(ParkingDTO parkingDTO) {
		this.parkingDTO = parkingDTO;
	}

 
	
	
}
