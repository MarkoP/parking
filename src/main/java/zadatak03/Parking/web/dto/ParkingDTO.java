package zadatak03.Parking.web.dto;

import java.util.ArrayList;
import java.util.List;

import zadatak03.Parking.model.Parking;

public class ParkingDTO {
	
	private Long id;
	private String name;
	private List<PlaceDTO> places = new ArrayList<PlaceDTO>();
	
	public ParkingDTO() {
		super();
	}

	public ParkingDTO(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public ParkingDTO(Parking parking) {
		this.id = parking.getId();
		this.name = parking.getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PlaceDTO> getPlaces() {
		return places;
	}

	public void setPlaces(List<PlaceDTO> places) {
		this.places = places;
	}
	
	
}
