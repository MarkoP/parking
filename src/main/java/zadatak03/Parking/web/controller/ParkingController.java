package zadatak03.Parking.web.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zadatak03.Parking.model.Parking;
import zadatak03.Parking.model.Place;
import zadatak03.Parking.service.ParkingService;
import zadatak03.Parking.service.PlaceService;
import zadatak03.Parking.web.dto.ParkingDTO;
import zadatak03.Parking.web.dto.PlaceDTO;

@RestController
public class ParkingController {
	@Autowired
	ParkingService parkingService;
	@Autowired
	PlaceService placeService;
	
/*	@RequestMapping(value = "api/parkings", method = RequestMethod.GET)
	public ResponseEntity<List<ParkingDTO>> getAllParkings(Pageable page){
		Page<Parking> parkings = parkingService.findAll(page);
		
	}*/
	
	@RequestMapping(value = "api/parkings", method = RequestMethod.GET)
	public ResponseEntity<List<ParkingDTO>> getAllParkings(){
		
		List<Parking> parkings = parkingService.findAll();
		List<ParkingDTO> retVal = convertToDto(parkings);
		
		return new ResponseEntity<List<ParkingDTO>>(retVal, HttpStatus.OK);
	}
	@RequestMapping(value = "api/parkings/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<ParkingDTO>> findOne(@PathVariable Long id){
		
		List<Parking> parkings = parkingService.pronadjiPoIdu(id);
		List<ParkingDTO> retVal = convertToDto(parkings);
		
		return new ResponseEntity<List<ParkingDTO>>(retVal, HttpStatus.OK); 
	}
	
	@RequestMapping(value = "api/parking/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PlaceDTO> savePlaceOnParking(@PathVariable Long id, @RequestBody PlaceDTO placeDTO){
		List<Parking> parkings = parkingService.pronadjiSlobodanParking();
		for (Parking parking : parkings) {
			System.out.println(parking.getId());
			System.out.println(parking.getPlaces().size());
			System.out.println(parking.getName());
		}
		Parking parking1 = new Parking();
		for (Parking parking : parkings) {
			if (parking.getId() == id) {
				parking1 = parking;
				System.out.println(parking1.getName());
				break;
			}else {
				return new ResponseEntity<PlaceDTO>(HttpStatus.NOT_FOUND);
			}
		}
		System.out.println(parking1.getName());
		Place place1 = new Place();
		for (Place place : parking1.getPlaces()) {
			if(parking1.getPlaces().size() != 0) {
				Random randomNumber = new Random();
				int number = randomNumber.nextInt(parking1.getPlaces().size());
			//	Place place1 = places.get(number);
				place1 = parking1.getPlaces().stream().collect(Collectors.toList()).get(number);
				
			}else {
				return new ResponseEntity<PlaceDTO>(HttpStatus.NOT_FOUND);
			}
		}
		place1.setFree(false);
		place1.setRegistration(placeDTO.getRegistration());
		place1.setPlace(parking1);
		System.out.println(place1.getNumber());
		parkingService.save(parking1);
		PlaceDTO placeDTO1 = new PlaceDTO(placeService.save(place1));
		
		return new ResponseEntity<PlaceDTO>(placeDTO1, HttpStatus.OK);
	}
	
	private List<ParkingDTO> convertToDto(List<Parking> parkings) {
		List<ParkingDTO> retVal = new ArrayList<ParkingDTO>();
		for (Parking parking : parkings) {
			ParkingDTO parkingDTO = new ParkingDTO(parking);
			for (Place place: parking.getPlaces()) {
				parkingDTO.getPlaces().add(new PlaceDTO(place));
			}
			retVal.add(parkingDTO);
		}
	return retVal;
	}
	
	
	
}
