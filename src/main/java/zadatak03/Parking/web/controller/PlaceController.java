package zadatak03.Parking.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import zadatak03.Parking.model.Place;
import zadatak03.Parking.service.PlaceService;
import zadatak03.Parking.web.dto.ParkingDTO;
import zadatak03.Parking.web.dto.PlaceDTO;

@RestController
public class PlaceController {

	@Autowired
	PlaceService placeService;
	
	@RequestMapping(value = "api/places", method = RequestMethod.GET)
	public ResponseEntity<List<PlaceDTO>> getAll(Pageable page){
		Page<Place> places = placeService.findAll(page);
		List<PlaceDTO> retVal = convertToDto(places.getContent());
		
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	private List<PlaceDTO> convertToDto(List<Place> places){
		if(places == null) {
			return null;
		}
		List<PlaceDTO> retVal = new ArrayList<PlaceDTO>();
		for (Place place : places) {
			PlaceDTO placeDTO = new PlaceDTO(place);
			
			placeDTO.setParkingDTO(new ParkingDTO(place.getPlace()));
			retVal.add(placeDTO);
		}
		return retVal;
	}
	
	@RequestMapping(value = "api/places/foreigners", method = RequestMethod.GET)
	public ResponseEntity<List<PlaceDTO>> getForeigners(){
		List<Place> places = placeService.foreignCars();
		List<PlaceDTO> retVal = convertToDto(places);
		if(retVal != null) {
			return new ResponseEntity<List<PlaceDTO>>(retVal, HttpStatus.OK);	
		}else {
		return new ResponseEntity<List<PlaceDTO>>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	
	
	
	
	
	
}
