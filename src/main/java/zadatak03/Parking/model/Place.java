package zadatak03.Parking.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Place {
	
	@Id
	@GeneratedValue
	private Long id;
	private int number;
	private boolean isFree;
	private String registration;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Parking parkingPlace;
	
	public Place() {
		super();
	}

	public Place(Long id, int number, boolean isFree, String registration) {
		super();
		this.id = id;
		this.number = number;
		this.isFree = isFree;
		this.registration = registration;
	}

	public Parking getPlace() {
		return parkingPlace;
	}

	public void setPlace(Parking place) {
		this.parkingPlace = place;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public boolean isFree() {
		return isFree;
	}

	public void setFree(boolean isFree) {
		this.isFree = isFree;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}
	
	
}
