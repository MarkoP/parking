package zadatak03.Parking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import zadatak03.Parking.model.Place;
@Component
public interface PlaceRepository extends JpaRepository<Place, Long> {
	
	//@Query("Select p from Place p left join fetch Parking t p.parkingPlace where ")
	
	@Query("select p from Place p where isFree=true")
	public List<Place> nadjiSlobodnoMesto();

	@Query("select p from Place p where isFree=false")
	public List<Place> findPlaceIsFreeFalse();
}
