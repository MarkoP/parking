package zadatak03.Parking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import zadatak03.Parking.model.Parking;

@Component
public interface ParkingRepository extends JpaRepository<Parking, Long> {
	
	
	@Query(value = "SELECT * FROM Parking", nativeQuery = true)
		public	List<Parking> pronadjiSveParkinge();
	
	@Query("Select p From Parking p where p.id = :id ")
	public List<Parking> nadjiParkingPoId(@Param("id") Long id);
	
	@Query("Select p From Parking p left join fetch p.places r where r.isFree = true")
	public List<Parking> pronadjiSlobodanParking();
	
}

